<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%hotel}}`.
 */
class m210226_190936_create_hotel_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'filename' => $this->string(),
            'filetype' => $this->string(),
            'value' => $this->text(), // base64 encoded strings
            'urls' => $this->text(), // json encoded string of possible urls
            'hotel_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%roomtype}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'urls' => $this->text(), // json encoded string of possible urls
            'hotel_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%hotel}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'zipcode' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
        ]);


        /* Link image table to hotel */
        $this->createIndex(
            'idx-image-hotel',
            'image',
            'hotel_id',
        );

        $this->addForeignKey(
            'fk-images-hotel',
            'image',
            'hotel_id',
            'hotel',
            'id',
            'CASCADE'
        );

        /* Link roomtype table to hotel */
        $this->createIndex(
            'idx-roomtype-hotel',
            'roomtype',
            'hotel_id',
        );

        $this->addForeignKey(
            'fk-roomtypes-hotel',
            'roomtype',
            'hotel_id',
            'hotel',
            'id',
            'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%image}}');
        $this->dropTable('{{%roomtype}}');
        $this->dropTable('{{%hotel}}');
    }
}
