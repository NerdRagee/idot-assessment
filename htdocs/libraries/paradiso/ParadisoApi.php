<?php

namespace app\libraries\paradiso;

use app\libraries\paradiso\HotelApi;
use app\models\Hotel;
use app\models\Image;
use app\models\Roomtype;

/**
 * TODO: importeerbaar maken
 * Simple function that joins elements into an url by
 * inserting "/" characters in between.
 */
function join_url($elements){
    return join("/", $elements);
}


/**
 * This class implements the paradiso API as documented 
 * by these[1] pages.
 * 
 * [1] http://tst.paradiso.w20e.com/api/v1/
 */
class ParadisoApi {
    private $version = 'v1';

    /**
     * The constructor can be used to supply a custom API url
     */
    public function __construct($url="tst.paradiso.w20e.com") {
        $this->base_url = join_url([$url, 'api', $this->version]);
    }

    /**
     * Gets the available hotels from remote API and stores
     * them in our database.
     * 
     * This function should have some more error handling, such as
     * when certain data would be missing from the API. Or if data
     * is suddenly of a different structure and cant be save in our
     * database. Right now we don't check for that and we crash
     * horribly if it goes wrong.
     * 
     * I think that handling should be added in the API classes. Maybe
     * define the response in some structure, I don't have experience
     * with that but I think something like xml(soap?) could work. And
     * then we just have to check against that structure.
     */
    public function getHotels(){
        $hotel_api = new HotelApi();
        $data = $hotel_api->get($this->base_url, null);
        foreach($data as $hotel_data){
            $hotel = Hotel::createOrUpdate(
                $hotel_data["id"],
                $hotel_data["name"],
                $hotel_data['zipcode'],
                $hotel_data["address"],
                $hotel_data["city"]
            );
            foreach($hotel_data["images"] as $image){
                Image::createOrUpdate(
                    $image["id"],
                    $image["image"]["filename"],
                    $image["image"]["filetype"],
                    $image["image"]["value"],
                    json_encode($image["_links"]),
                    $hotel->id);
            }
            foreach($hotel_data["roomtypes"] as $roomtype){
                Roomtype::createOrUpdate(
                    $roomtype["id"],
                    $roomtype["name"],
                    json_encode($roomtype["_links"]),
                    $hotel->id
                );
            }
            //Roomtype::createOrUpdate($hotel->id);
        }
    }

    public function getHotel($id){

    }
}