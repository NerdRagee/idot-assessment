<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hotel".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $zipcode
 * @property string $city
 *
 * @property Image[] $images
 * @property Roomtype[] $roomtypes
 */
class Hotel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'zipcode', 'city'], 'required'],
            [['name', 'address', 'zipcode', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'zipcode' => 'Zipcode',
            'city' => 'City',
        ];
    }

    /**
     * Gets query for [[Images]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['hotel_id' => 'id']);
    }

    /**
     * Gets query for [[Roomtypes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoomtypes()
    {
        return $this->hasMany(Roomtype::className(), ['hotel_id' => 'id']);
    }


    /**
     * Create or update a database entry. Input data
     * is data received from an external API
     * 
     * @return app\models\Hotel
     */
    public static function createOrUpdate($id, $name, $zipcode, $address, $city){
        $hotel = Hotel::find()->where(['id' => $id])->one();
        if(!$hotel){
            $hotel = new Hotel();
            $hotel->id = $id;
        }
        $hotel->name = $name;
        $hotel->zipcode = $zipcode;
        $hotel->address = $address;
        $hotel->city = $city;
        if(!$hotel->save()){
            // doe iets meer met de error
        }
        return $hotel;
    }
}
