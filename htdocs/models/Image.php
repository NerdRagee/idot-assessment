<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string|null $filename
 * @property string|null $filetype
 * @property string|null $value
 * @property string|null $urls
 * @property int $hotel_id
 *
 * @property Hotel $hotel
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'urls'], 'string'],
            [['hotel_id'], 'required'],
            [['hotel_id'], 'integer'],
            [['filename', 'filetype'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'filetype' => 'Filetype',
            'value' => 'Value',
            'urls' => 'Urls',
            'hotel_id' => 'Hotel ID',
        ];
    }

    /**
     * Gets query for [[Hotel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }

    /**
     * Create or update image data in db
     * 
     * @return app\models\Image
     */
    public static function createOrUpdate($id, $filename, $filetype, $value, $urls, $hotel_id){
        $image = Image::find()->where(['id' => $id])->one();
        if(!$image){
            $image = new Image;
            $image->id = $id;
        }
        $image->filename = $filename;
        $image->filetype = $filetype;
        $image->value = $value;
        $image->urls = $urls;
        $image->hotel_id = $hotel_id;
        if(!$image->save()){
            // doe iets met de error
        }
        return $image;
    }
}
