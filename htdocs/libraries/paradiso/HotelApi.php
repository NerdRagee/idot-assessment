<?php

namespace app\libraries\paradiso;

use app\libraries\rest\HttpRestApi;


/**
 * This class implements the Paradiso Hotel API
 * 
 * For reference see the documentation[1]
 * 
 * [1] http://tst.paradiso.w20e.com/api/v1/hotels/
 */
class HotelApi extends HttpRestApi {

    // The name of the url endpoint
    private $endpoint = 'hotels/';

    // The paradiso test API is without tls
    protected $tls_enabled = False;

    /**
     * Gets all the available hotels
     * 
     * @param string $url The url 
     * @param string $data the data to be sent with the request
     */
    public function get($base_url, $data) {
        $url = join_url([$base_url, $this->endpoint]);
        return parent::_get($url, $data);
    }
}