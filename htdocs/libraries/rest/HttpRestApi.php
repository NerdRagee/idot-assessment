<?php

namespace app\libraries\rest;

use app\exceptions\NotImplementedException;
use app\libraries\rest\RestApi;


/**
 * A base implementation of an HTTP rest API. 
 * 
 * All the desired (and supported) REST methods should be implemented by the child
 * class implementation. This class simply adds the support for making the actual
 * HTTP calls.
 */
class HttpRestApi implements RestApi {
    protected $tls_enabled = True;
    protected $json_api = True;

    /**
     * Each HttpRestApi object can be provided with a curl session
     * instance, so it can be reused. Otherwise a new session is created
     * for each request
     */
    public function __construct($curlsession = null){
        $this->_curl = $curlsession;
    }

    /**
     * Perform the actual http request
     */
    private function _perform_request($url, $data, $headers=null, $curlopt_cb=null){
        $do_close = False;
        if(!$this->_curl){
            $this->_curl = curl_init();
            $do_close = True;
        }

        curl_setopt($this->_curl, CURLOPT_URL, $url);
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
        if($headers){
            curl_setopt($this->_curl, CURLOPT_HTTPHEADER, $headers);
        }
        if($curlopt_cb){
            // Add curlopt settings depending on the call being made
            $curlopt_cb();
        }
        
        $data = curl_exec($this->_curl);
        if($do_close){
            curl_close($this->_curl);
        }
        return $data;
    }

    /**
     * Override this in child class to implement API endpoint.
     */
    public function get($url, $data){
        throw new NotImplementedException("This API endpoint is not supported");
    }

    /**
     * Implements http GET requests
     */
    protected function _get($url, $data){
        if($this->json_api){
            $headers = [
                'Content-Type: application/json',
                'Accept: application/json'
            ];
        }
        $data = $this->_perform_request($url, $data, $headers=$headers);
        if($this->json_api){
            $data = json_decode($data, true);
        }
        return $data;
    }

    /**
     * Override this in child class to implement API endpoint.
     */
    public function post($url, $data){
        throw new NotImplementedException("This API endpoint is not supported");
    }

    /**
     * Override this in child class to implement API endpoint.
     */
    public function delete($url, $data){
        throw new NotImplementedException("This API endpoint is not supported");
    }

    /**
     * Override this in child class to implement API endpoint.
     */
    public function put($url, $data){
        throw new NotImplementedException("This API endpoint is not supported");
    }
}