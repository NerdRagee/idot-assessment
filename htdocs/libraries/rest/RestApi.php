<?php

namespace app\libraries\rest;


/**
 * Simple interface implementing basic REST api functionalities.
 * This can be implemented for any API but most common is HTTP.
 * 
 */
interface RestApi {
    public function post($endpoint, $data);
    public function get($endpoint, $data);
    public function delete($endpoint, $data);
    public function put($endpoint, $data);
}
