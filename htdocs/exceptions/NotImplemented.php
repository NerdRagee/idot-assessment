<?php

namespace app\exceptions;

use Exception;

/**
 * An exception indicating that a function is not intentionally
 * not implemented.
 */
class NotImplementedException extends Exception {
    /**
     * The class doesn't have any custom implementations. Mainly the
     * name of the exception indicates enough.
     */
}