<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roomtype".
 *
 * @property int $id
 * @property string $name
 * @property string|null $urls
 * @property int $hotel_id
 *
 * @property Hotel $hotel
 */
class Roomtype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roomtype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'hotel_id'], 'required'],
            [['urls'], 'string'],
            [['hotel_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urls' => 'Urls',
            'hotel_id' => 'Hotel ID',
        ];
    }

    /**
     * Gets query for [[Hotel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }


    /**
     * Create or update a roomtype
     * 
     * @return app\models\Roomtype
     */
    public static function createOrUpdate($id, $name, $urls, $hotel_id){
        $roomtype = Roomtype::find()->where(['id'=> $id])->one();
        if(!$roomtype){
            $roomtype = new Roomtype();
            $roomtype->id = $id;
        }
        $roomtype->name = $name;
        $roomtype->urls = $urls;
        $roomtype->hotel_id = $hotel_id;

        if(!$roomtype->save()){
            // doe iets met de error
        }
        return $roomtype;
    }
}
